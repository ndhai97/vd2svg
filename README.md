# VD2SVG

Lately I been doing some mod on SystemUI and found that Android using Vector Drawable instead of SVG to display icon, so I write this small script to turn Vector Drawable to SVG so that I can easily view and edit it to fit my need

The method that I use to write this script are taken from here: https://stackoverflow.com/questions/44948396/convert-vectordrawable-to-svg

## Usage:

Create **xml** and **svg** folder then put vector drawable to xml folder after that execute ./run.sh shell script. Svg results will be save in svg folder
