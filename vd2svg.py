#!/usr/bin/env python

import sys
import xml.etree.ElementTree as ET

SVG_PATH = sys.argv[1]

tree = ET.parse(SVG_PATH)
vector_tag = tree.getroot()
svg_tag = ET.Element("svg", attrib = { "xmlns": "http://www.w3.org/2000/svg", "width": "24", "height": "24", "viewBox": "0 0 24 24"})
drawable_namespace = '{http://schemas.android.com/apk/res/android}'

# SVG Path builder
list_of_path = vector_tag.findall('path')

if not list_of_path:
    exit()

for path in list_of_path:
    path_data = path.get(drawable_namespace + 'pathData')
    new_path = ET.Element("path", attrib = { "fill": "#000000", "d": path_data })
    svg_tag.append(new_path)

# Print to std output
print(ET.tostring(svg_tag, encoding='utf8', method='xml').decode())
