#!/usr/bin/env bash

for file in $(ls ./xml/*); do
  if [ ! -z "$(./vd2svg.py "$file")" ]; then
    ./vd2svg.py "$file" > ./svg/"$(basename "$file" .xml)".svg
  fi
done
